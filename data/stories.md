## je suis un robot
* jesuisunrobot
  - utter_jesuisunrobot

## au revoir
* aurevoir
  - utter_aurevoir

## homepage
* greet
  - utter_greet
* homepage
  - utter_homepage
* affirm
  - utter_aurevoir

## domicile_biens_argent
* greet
  - utter_greet
* domicile_biens_argent
  - utter_domicile_biens_argent
* affirm
  - utter_aurevoir

## enfants
* greet
  - utter_greet
* enfants
  - utter_enfants
* affirm
  - utter_aurevoir

# divorce
* greet
  - utter_greet
* divorce
  - utter_divorce
* affirm
  - utter_aurevoir

## tribunal
* greet
  - utter_greet
* tribunal
  - utter_tribunal
* affirm
  - utter_aurevoir

